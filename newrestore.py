#!/usr/bin/python
"""
 Possible arguments: 
   --nodb: Will not refresh the database
   --nocode: Will not refresh the codebase
   --trail: will not overwrite the log file every time (exec 2>&1 to exec 2>>&1)

Uses OptParse (instead of argparse) for compatibility


Refreshes test sites with command line arguments for disabling 
"""

# Imports
import logging
from optparse import OptionParser
import sys, os, pwd
import subprocess
import glob

# Configure logging
FORMAT =  '%(asctime)-15s %(message)s'

# Parse Options
parser = OptionParser()
parser.add_option("--nodb", "--nodatabase", action="store_true", dest="nodb", default=False, help="Use this argument to not refresh the database")
parser.add_option("--nocode", "--nocodebase", action="store_true", dest="nocode", default=False, help="Use this argument to not refresh the code base")
parser.add_option("--trail", "--appendlog", action="store_true", dest="appendlog", default=False, help="Use this argument to not truncate the log file on run")

(options, args) = parser.parse_args()

# Handle trail option before the loop starts
if options.appendlog:
	logging.basicConfig(filename='Xrefresh.log',format=FORMAT,level=logging.DEBUG)
else:
	logging.basicConfig(filename='Xrefresh.log',filemode='w',format=FORMAT,level=logging.DEBUG)

def cleanup():
	logging.info("-"*20)
	sys.exit(1)

def get_username():
	return pwd.getpwuid( os.getuid() )[0]

def refresh_code(user=get_username()):
	# refresh da code
	logging.info("Beginning code refresh for %s" % user)
	productionpath = "/home/%s/public_html/focus/" % user
	testingpath = "/home/%s/public_html/testing/" % user

	if not os.path.isdir(productionpath):
		logging.warning("Production path is not what we expected! Trying one other...")
		productionpath = "/home/%s/public_html/focusv5/" % user
		if not os.path.isdir(productionpath):
			logging.warning("Failed to find production!! Failing out!")
			cleanup()

	prodconfig="%s/config.inc.php" % productionpath

	if not os.path.exists(prodconfig):
		logging.warning("Cannot find config.inc.php!")
		cleanup()

	logging.info("refreshing code from %s to %s" % (productionpath, testingpath))
	try:
		scmd = ['/usr/bin/rsync', '-av', '--delete', productionpath, testingpath, '--exclude=modules/School_Setup', '--exclude=config.inc.php', '--exclude=assets/themes/Default/logo.png', '--exclude=.htaccess']
		ret = subprocess.Popen(scmd, shell=False, stdout=subprocess.PIPE).communicate()[0].splitlines()
		for line in ret:
			if line.startswith("sent"):
				logging.info(line)
			if line.startswith("total size"):
				logging.info(line)
	except subprocess.CalledProcessError as e:
		ret = e.returncode
		if ret in (1, 2):
			logging.warning("REFRESH FAILED! EXITING!")
		elif ret in (3, 4, 5):
			logging.warning("CRITICAL ERROR: EXITING!")
		cleanup()

	logging.info("Code refresh complete!")
	
def dbrun(dbuser, dbname, dbhost, dbpwd, testuser, testname, testhost, testpwd, user=get_username()):
	# pg_dump -hproddbhost -Uproddbuser proddbname -Fc -T user_audit_trail -T login_history -T students_changelog | pg_restore -htestdbhost -Utestdbuser -dtestdbname -c
	dumpuser = "-U%s" % dbuser
	dumphost = "-h%s" % dbhost
	restuser = "-U%s" % testuser
	resthost = "-h%s" % testhost
	restname = "-d%s" % testname

	dumpcmd = ['pg_dump', dumphost, dumpuser, dbname, '-Fc', '--clean', '-T', 'user_audit_trail', '-T', 'login_history', '-T', 'students_changelog']
	restorecmd = ['pg_restore', resthost, restuser, restname, '-c']

	logging.info("Beginning database refresh. This may take some time...")
	try:
		ps = subprocess.Popen(dumpcmd, stdout=subprocess.PIPE)
		output = subprocess.Popen(restorecmd, stdin=ps.stdout, stderr=subprocess.STDOUT, stdout=subprocess.PIPE).communicate()[0]
		exit = ps.wait()
		with open("refresherror.log", "w") as errorlog:
			for line in output:
				errorlog.write(line)
				
	except subprocess.CalledProcessError as e:
		logging.warning("Something went wrong with the database refresh! This may be a false alarm due to postgres generating garbage errors")
		logging.warning(e)
		cleanup()

	logging.info("Refresh complete! Check refresherror.log for any issues.")
	return exit


def get_config(path):

	config="%s/config.inc.php" % path

	if not os.path.exists(config):
		logging.warning("Cannot find config.inc.php!")
		cleanup()

	try:
		with open(config) as inf:
			for line in inf:
				if line.startswith("$DatabaseServer"):
					try:
						proddbhost = line.split("= ")[1].split("'")[1]
					except:
						proddbhost = line.split("= ")[1].split('"')[1]
				if line.startswith("$DatabaseName"):
					try:
						proddbname = line.split("= ")[1].split("'")[1]
					except:
						proddbname = line.split("= ")[1].split('"')[1]
				if line.startswith("$DatabaseUsername"):
					try:
						proddbuser = line.split("= ")[1].split("'")[1]
					except:
						proddbuser = line.split("= ")[1].split('"')[1]
				if line.startswith("$DatabasePassword"):
					try:
						proddbpwd = line.split("= ")[1].split("'")[1]
					except:
						proddbpwd = line.split("= ")[1].split('"')[1]
	except IOError:
		logging.warning("Could not open config.inc.php! Failing out!")

	if (not proddbuser) or (not proddbname) or (not proddbpwd):
		logging.warning("Could not parse config file. No source database known!")
		cleanup()

	return proddbuser, proddbname, proddbhost, proddbpwd

def refresh_db(user=get_username()):
	proddbuser=None
	proddbhost="localhost"
	proddbname=None
	proddbpwd=None
	testuser=None
	testhost="localhost"
	testname=None
	testpwd=None

	# refresh da code
	logging.info("Beginning Database refresh setup for %s" % user)
	productionpath = "/home/%s/public_html/focus" % user
	testingpath = "/home/%s/public_html/testing" % user

	if not os.path.isdir(productionpath):
		logging.warning("Production path is not what we expected! Trying one other...")
		productionpath = "/home/%s/public_html/focusv5" % user
		if not os.path.isdir(productionpath):
			logging.warning("Failed to find production!! Failing out!")
			cleanup()

	(proddbuser, proddbname, proddbhost, proddbpwd) = get_config(productionpath)
	(testuser, testname, testhost, testpwd) = get_config(testingpath)

	if proddbname == testname:
		logging.warning("Testing config is the same as production! ABORTING")
		cleanup()

	print dbrun(proddbuser, proddbname, proddbhost, proddbpwd, testuser, testname, testhost, testpwd)

# Primary loop
def Main():
	logging.info("-"*20)
	logging.info("BEGIN SCHEDULED REFRESH FOR %s" % get_username())
	
	logging.info("Don't refresh db: %s" % options.nodb)
	logging.info("Don't refresh code: %s" % options.nocode)
	logging.info("Keep log: %s" % options.appendlog)

	if not options.nocode:
		refresh_code()

	if not options.nodb:
		refresh_db()

	logging.info("It appears your work is complete!")
	logging.info("-"*20)

# Set up loop
if __name__ == '__main__':
	Main()
